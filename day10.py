from collections import deque

def main_1():
	in_list = [int(x) for x in read_file('./input10.txt')]
	in_list.sort()
	jumps_1 = 0
	jumps_3 = 1
	prev = 0
	for cur in in_list:
		if (cur - prev) == 1:
			jumps_1 += 1
		elif (cur - prev) == 3:
			jumps_3 += 1
		prev = cur

	print(jumps_1 * jumps_3) 

def main_2():
	in_list = [int(x) for x in read_file('./input10.txt')]
	in_list.sort()
	# we start at joltage 0, and end 3 higher than the last capacitor
	in_list = [0] + in_list + [in_list[-1] + 3]
	# arrangements[i] is the number of paths to get to in_list[i]
	arrangements = [0] * len(in_list)
	arrangements[0] = 1

	for i in range(1, len(in_list)):
		lookback = min(i, 3)
		for j in range(i - lookback, i):
			if in_list[i] - in_list[j] <= 3:
				arrangements[i] += arrangements[j]
				
	print(arrangements[-1])


def parse_line(line):
	pass

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

def read_file_groups(filename):
	in_list = [[]]
	with open(filename, 'r') as file:
		for line in file:
			if line.strip() == '':
				in_list.append([])
			else:
				in_list[-1].append(line.strip())
	return in_list

if __name__ == "__main__":
	main_2()