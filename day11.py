from collections import deque

def main_1():
	grid = [[c for c in line] for line in read_file('./input11.txt')]

	stable = False
	while not stable:
		stable = True
		new_grid = [[seat for seat in row] for row in grid]
		for y in range(0, len(grid)):
			for x in range(0, len(grid[0])):
				current = grid[y][x]
				if current == '.':
					continue
				count = count_neighbors_1(grid, x, y)
				if current == 'L' and count == 0:
					stable = False
					new_grid[y][x] = '#'
				elif current == '#' and count >= 4:
					stable = False
					new_grid[y][x] = 'L'
		grid = new_grid

	count = 0
	for row in grid:
		for seat in row:
			if seat == '#':
				count += 1
	print(count)  

def count_neighbors_1(grid, x, y):
	min_x = max(x-1, 0)
	max_x = min(x+1, len(grid[0]) - 1)
	min_y = max(y-1, 0)
	max_y = min(y+1, len(grid) - 1)
	count = 0
	for y_i in range(min_y, max_y + 1):
		for x_i in range(min_x, max_x + 1):
			if x_i == x and y_i == y:
				continue
			else:
				count += grid[y_i][x_i] == '#'
	return count

def main_2():
	grid = [[c for c in line] for line in read_file('./input11.txt')]

	stable = False
	while not stable:
		stable = True
		new_grid = [[seat for seat in row] for row in grid]
		for y in range(0, len(grid)):
			for x in range(0, len(grid[0])):
				current = grid[y][x]
				if current == '.':
					continue
				count = count_neighbors_2(grid, x, y)
				if current == 'L' and count == 0:
					stable = False
					new_grid[y][x] = '#'
				elif current == '#' and count >= 5:
					stable = False
					new_grid[y][x] = 'L'
		grid = new_grid

	count = 0
	for row in grid:
		for seat in row:
			if seat == '#':
				count += 1
	print(count)  

def count_neighbors_2(grid, x, y):
	count = 0
	for x_dir in [-1, 0, 1]:
		for y_dir in [-1, 0, 1]:
			if x_dir == 0 and y_dir == 0:
				continue
			x_i = x + x_dir
			y_i = y + y_dir
			while not is_wall(grid, x_i, y_i):
				if grid[y_i][x_i] == 'L':
					break
				elif grid[y_i][x_i] == '#':
					count += 1
					break
				x_i += x_dir
				y_i += y_dir

	return count

def is_wall(grid, x, y):
	if x < 0 or x >= len(grid[0]):
		return True
	if y < 0 or y >= len(grid):
		return True


def parse_line(line):
	pass

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

def read_file_groups(filename):
	in_list = [[]]
	with open(filename, 'r') as file:
		for line in file:
			if line.strip() == '':
				in_list.append([])
			else:
				in_list[-1].append(line.strip())
	return in_list

if __name__ == "__main__":
	main_2()