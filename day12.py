import math

def main_1():
	in_list = [parse_line(line) for line in read_file('./input12.txt')]
	x = 0.
	y = 0.
	heading = 90
	heading_map = {0: [0, 1], 90: [1, 0], 180: [0, -1], 270: [-1, 0]}

	for inst in in_list:
		if inst[0] == 'E':
			x += inst[1]
		if inst[0] == 'N':
			y += inst[1]
		if inst[0] == 'W':
			x -= inst[1]
		if inst[0] == 'S':
			y -= inst[1]
		if inst[0] == 'L':
			heading = (heading - inst[1]) % 360
		if inst[0] == 'R':
			heading = (heading + inst[1]) % 360
		if inst[0] == 'F':
			x += inst[1] * heading_map[heading][0]
			y += inst[1] * heading_map[heading][1]
		print((x, y, heading))

	distance = abs(x) + abs(y)
	print(distance)

def main_2():
	in_list = [parse_line(line) for line in read_file('./input12.txt')]
	x = 0
	y = 0
	w_x = 10
	w_y = 1
	rot_map = {90: [[0, -1], [1, 0]], 180: [[-1, 0], [0, -1]], 270: [[0, 1], [-1, 0]]}

	for inst in in_list:
		if inst[0] == 'E':
			w_x += inst[1]
		elif inst[0] == 'N':
			w_y += inst[1]
		elif inst[0] == 'W':
			w_x -= inst[1]
		elif inst[0] == 'S':
			w_y -= inst[1]
		elif inst[0] == 'L' or inst[0] == 'R':
			val = inst[1] if inst[0] == 'L' else 360-inst[1]
			old_w_x = w_x
			old_w_y = w_y
			matrix = rot_map[val]
			w_x = old_w_x * matrix[0][0] + old_w_y * matrix[0][1]
			w_y = old_w_x * matrix[1][0] + old_w_y * matrix[1][1]
		elif inst[0] == 'F':
			x += w_x * inst[1]
			y += w_y * inst[1]
		print((x, y, w_x, w_y))

	distance = abs(x) + abs(y)
	print(distance)


def parse_line(line):
	return (line[0], int(line[1:]))

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

def read_file_groups(filename):
	in_list = [[]]
	with open(filename, 'r') as file:
		for line in file:
			if line.strip() == '':
				in_list.append([])
			else:
				in_list[-1].append(line.strip())
	return in_list

if __name__ == "__main__":
	main_2()