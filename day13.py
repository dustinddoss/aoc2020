def main_1():
	in_list = read_file('./input13.txt')
	cur_time = int(in_list[0])
	buses = [int(num) for num in in_list[1].split(',') if num != 'x']
	min_mod = buses[0] - cur_time % buses[0]
	min_id = buses[0]
	for bus in buses:
		print(bus)
		if (bus - cur_time % bus) < min_mod:
			min_mod = bus - cur_time % bus
			min_id = bus
		print((min_mod, min_id))
	print(min_mod * min_id)

def main_2():
	in_list = read_file('./input13.txt')[1].split(',')
	buses = []
	for i in range(len(in_list)):
		if in_list[i] != 'x':
			buses.append((int(in_list[i]), i))

	product = 1
	val = 0
	for bus in buses:
		cur_mod = val % bus[0]
		prod_mod = product % bus[0]
		goal_mod = -1*bus[1] % bus[0]
		multiple = 0
		while cur_mod != goal_mod:
			cur_mod = (cur_mod + prod_mod) % bus[0]
			multiple += 1
		val = val + multiple * product
		product *= bus[0]

	for bus in buses:
		assert  (bus[0] - val) % bus[0] == (bus[1] % bus[0])
	print(val)

def parse_line(line):
	return (line[0], int(line[1:]))

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

def read_file_groups(filename):
	in_list = [[]]
	with open(filename, 'r') as file:
		for line in file:
			if line.strip() == '':
				in_list.append([])
			else:
				in_list[-1].append(line.strip())
	return in_list

if __name__ == "__main__":
	main_2()