def main_1():
	in_list = read_file('./input14.txt')
	mask = []
	memory = {}
	for line in in_list:
		if line[0:4] == 'mask':
			mask = parse_mask(line)
		elif line[0:3] == 'mem':
			addr, val_str = parse_mem(line)
			val_str = apply_mask(val_str, mask)
			memory[addr] = val_str
	sum = 0
	for key in memory.keys():
		sum += int(memory[key], 2)
	print(sum)

def main_2():
	in_list = read_file('./input14.txt')
	mask = format(0, '036b')
	memory = {}
	for line in in_list:
		if line[0:4] == 'mask':
			mask = line[7:]
		elif line[0:3] == 'mem':
			parsed_addr, val_str = parse_mem(line)
			binstr_addr = format(int(parsed_addr), '036b')
			addr_list = apply_mask_v2(binstr_addr, mask)
			for addr in addr_list:
				memory[addr] = val_str
	sum = 0
	for key in memory.keys():
		sum += int(memory[key], 2)
	print(sum)

def apply_mask_v2(orig_addr, mask):
	addr_list = [[]]
	for i in range(0, 36):
		if mask[i] == '0':
			for addr in addr_list:
				addr.append(orig_addr[i])
		elif mask[i] == '1':
			for addr in addr_list:
				addr.append('1')
		elif mask[i] == 'X':
			new_list = []
			for addr in addr_list:
				new_list.append(addr + ['0'])
				new_list.append(addr + ['1'])
			addr_list = new_list
	int_list = [int(''.join(addr), 2) for addr in addr_list]
	return int_list


def parse_mask_v2(line):
	mask_str = line[7:]
	assert len(mask_str) == 36
	mask = []
	for i in range(0, 36):
		if mask_str[i] == 'X':
			continue
		else:
			mask.append((i, mask_str[i]))

	return mask

def apply_mask(val, mask):
	for (index, mask_val) in mask:
		val = val[:index] + mask_val + val[index+1:]
	return val

def parse_mem(line):
	line_list = line.split(' ')  # ['mem[###]', '=', 'dec val']
	mem_addr = line_list[0][4:-1]
	mem_val = int(line_list[2])
	mem_val_str = format(mem_val, '036b')
	return (mem_addr, mem_val_str)

def parse_mask(line):
	mask_str = line[7:]
	assert len(mask_str) == 36
	mask = []
	for i in range(0, 36):
		if mask_str[i] == 'X':
			continue
		else:
			mask.append((i, mask_str[i]))

	return mask

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

def read_file_groups(filename):
	in_list = [[]]
	with open(filename, 'r') as file:
		for line in file:
			if line.strip() == '':
				in_list.append([])
			else:
				in_list[-1].append(line.strip())
	return in_list

if __name__ == "__main__":
	main_2()