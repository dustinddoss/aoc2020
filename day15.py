import math
import collections
import time

def main():
	in_list = [2,20,0,4,1,17]
	target_turn = 30000000
	
	turn_map = {in_list[i] : i for i in range(len(in_list) - 1)}
	turn = len(in_list)
	last_num = in_list[-1]

	while turn < target_turn:
		if last_num in turn_map:
			new_num = (turn - 1 - turn_map[last_num])
		else:
			new_num = 0
		turn_map[last_num] = turn - 1
		last_num = new_num
		turn += 1
	print(last_num)

def parse_line(line):
	return line

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

def read_file_groups(filename):
	in_list = [[]]
	with open(filename, 'r') as file:
		for line in file:
			if line.strip() == '':
				in_list.append([])
			else:
				in_list[-1].append(line.strip())
	return in_list

if __name__ == "__main__":
        main()
