import math
import collections
import time

def main_1():
	in_list = read_file_groups('input16.txt')
	rules_str_list = in_list[0]
	other_tickets = in_list[2][1:]

	rules = []
	for rule_str in rules_str_list:
		rules.extend(parse_rule(rule_str)[1])
	
	error_rate = 0
	for ticket in other_tickets:
		for field in ticket.split(","):
			if not matches_any_rule(int(field), rules):
				error_rate += int(field)

	print(error_rate)
	
# "field" is an integer value, one of many in a ticket
# "rule" is a list of ranges in the form [[start_1, end_1], [start_2, end_2]]
# "label" is the string label corresponding to a rule
# "index" is the index in the list of ticket fields. Each label should map to 1 index.
def main_2():
	in_list = read_file_groups('input16.txt')
	rules_str_list = in_list[0]
	my_ticket = [int(field) for field in in_list[1][1].split(',')]
	other_tickets = [[int(field) for field in ticket.split(',')] for ticket in in_list[2][1:]]

	rules = {}
	for rule_str in rules_str_list:
		parsed_rule = parse_rule(rule_str)
		rules[parsed_rule[0]] = parsed_rule[1]
	
	valid_tickets = []
	for ticket in other_tickets:
		is_valid = True
		for field in ticket:
			field_matches = False
			for rule in rules.values():
				if matches_rule(field, rule):
					field_matches = True
					break
			if not field_matches:
				is_valid = False
				break
		if is_valid:
			valid_tickets.append(ticket)

	num_fields = len(rules)
	assert(len(rules) == len(valid_tickets[0]))
	field_index_set_map = {key : set(range(0, num_fields)) for key in rules.keys()}

	for ticket in valid_tickets:
		for i in range(num_fields):
			field = ticket[i]
			for label, rule in rules.items():
				if not matches_rule(field, rule):
					field_index_set_map[label].discard(i)


	field_index_map = {}
	while (len(field_index_map) < num_fields):
		to_delete_labels = set()
		for label, index_set in field_index_set_map.items():
			if len(index_set) == 1:
				index = index_set.pop()
				field_index_map[label] = index
				to_delete_labels.add(label)
				for other_label in field_index_set_map.keys():
					field_index_set_map[other_label].discard(index)

		for label in to_delete_labels:
			del field_index_set_map[label]

	product = 1
	for label, index in field_index_map.items():
		if label.startswith("departure"):
			product *= my_ticket[index]
	print(product)



def parse_rule(line):
	label, ranges_str = line.split(":")
	ranges_str = ranges_str.strip()
	ranges_str_list = ranges_str.split(" or ")
	ranges = [[int(num) for num in range.split("-")] for range in ranges_str_list]
	return (label, ranges)

def matches_rule(val, rule):
	for rule_range in rule:
		if val >= rule_range[0] and val <= rule_range[1]:
			return True
	return False

def matches_any_rule(val, rules):
	for rule in rules:
		if val >= rule[0] and val <= rule[1]:
			return True
	return False 


def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

def read_file_groups(filename):
	in_list = [[]]
	with open(filename, 'r') as file:
		for line in file:
			if line.strip() == '':
				in_list.append([])
			else:
				in_list[-1].append(line.strip())
	return in_list

if __name__ == "__main__":
        main_2()
