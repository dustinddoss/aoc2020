import math
import collections
import time

def main_1():
	in_list = [parse_line(line) for line in read_file('input17.txt')]
	world_set = set()
	for i in range(len(in_list)):
		for j in range(len(in_list[0])):
			if in_list[i][j]:
				world_set.add((i, j, 0, 0))
	for cycle in range(0, 6):
		new_world_set = set()
		checked = set()
		for center in world_set:
			for coord in get_neighbors(center):
				if coord in checked:
					continue
				checked.add(coord)
				active_count = count_active_neighbors(coord, world_set)
				if coord in world_set:
					if active_count in [2, 3]:
						new_world_set.add(coord)
				else:
					if active_count == 3:
						new_world_set.add(coord)
		world_set = new_world_set

	print(len(world_set))




def main_2():
	in_list = [parse_line(line) for line in read_file('input17.txt')]
	world_set = set()
	for i in range(len(in_list)):
		for j in range(len(in_list[0])):
			if in_list[i][j]:
				world_set.add((i, j, 0, 0))
	for cycle in range(0, 6):
		new_world_set = set()
		checked = set()
		for center in world_set:
			for coord in get_neighbors(center, 4):
				if coord in checked:
					continue
				checked.add(coord)
				active_count = count_active_neighbors(coord, world_set, 4)
				if coord in world_set:
					if active_count in [2, 3]:
						new_world_set.add(coord)
				else:
					if active_count == 3:
						new_world_set.add(coord)
		world_set = new_world_set

	print(len(world_set))

def get_neighbors(center, dimensions = 3):
	coords_set = set([center])
	for d in range(dimensions):
		new_coords = set()
		for coord in coords_set:
			for offset in [-1, 1]:
				new_coord = list(coord)
				new_coord[d] += offset
				new_coords.add(tuple(new_coord))
		for new_coord in new_coords:
			coords_set.add(new_coord)
	return coords_set


def count_active_neighbors(center, world_set, dimensions = 3):
	count = 0
	for coord in get_neighbors(center, dimensions):
		if coord != center and (coord in world_set):
			count += 1
	return count

def parse_line(line):
	return [c == '#' for c in line]

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

def read_file_groups(filename):
	in_list = [[]]
	with open(filename, 'r') as file:
		for line in file:
			if line.strip() == '':
				in_list.append([])
			else:
				in_list[-1].append(line.strip())
	return in_list

if __name__ == "__main__":
        main_2()
