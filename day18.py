import math
import collections
import time

def main_1():
	in_list = [parse_line(line) for line in read_file('input18.txt')]
	cum_sum = 0
	for line in in_list:
		cum_sum += evaluate(line)
	print(cum_sum)
	
def evaluate(expr):
	print(expr)
	ints = set([str(x) for x in range(0, 10)])
	index = 0
	val = 0
	operator = ''

	if expr[index] == '(':
		val, end = evaluate(expr[1:])
		index = index + end + 2
	else:
		val = int(expr[index])
		index += 1

	while index < len(expr):
		if expr[index] == ')':
			return val, index
		elif expr[index] == ' ':
			pass
		elif expr[index] in ints:
			if operator == '+':
				val = val + int(expr[index])
			elif operator == '*':
				val = val * int(expr[index])
			else:
				assert False
			operator = ''
		elif expr[index] in ['+', '*']:
			operator = expr[index]
		else:
			assert expr[index] == '('
			subval, end = evaluate(expr[index + 1:])
			index += end + 1
			if operator == '+':
				val = val + subval
			elif operator == '*':
				val = val * subval
			else:
				assert False
			operator = ''
		index += 1

	return val

def evaluate_advanced(expr):
	ints = set([str(x) for x in range(0, 10)])

	no_parens_expr_list = []
	i = 0
	while i < len(expr):
		c = expr[i]
		if c == '(':
			end = find_paren_end(expr, i)
			val = evaluate_advanced(expr[i+1:end])
			no_parens_expr_list.append(val)
			i = end
		else:
			if c != ' ':
				no_parens_expr_list.append(c)
		i += 1

	no_adds_expr_list = []
	i = 0
	while i < len(no_parens_expr_list):
		if no_parens_expr_list[i] == '+':
			no_adds_expr_list[-1] += int(no_parens_expr_list[i+1])
			i += 1
		elif no_parens_expr_list[i] == '*':
			no_adds_expr_list.append('*')
		else:
			no_adds_expr_list.append(int(no_parens_expr_list[i]))
		i += 1

	val = 1
	for entry in no_adds_expr_list:
		if entry != '*':
			if not isinstance(entry, int):
				print(no_adds_expr_list)
			val *= entry

	return val 



def find_paren_end(expr, start):
	count = 0
	assert expr[start] == '('
	index = start + 1
	while index < len(expr):
		if expr[index] == ')':
			if count == 0:
				return index
			else:
				count -= 1
		if expr[index] == '(':
			count += 1
		index += 1	
	print(expr)

	assert False

def main_2():
	in_list = [parse_line(line) for line in read_file('input18.txt')]
	cum_sum = 0
	for line in in_list:
		cum_sum += evaluate_advanced(line)
	print(cum_sum)

def parse_line(line):
	return line

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

def read_file_groups(filename):
	in_list = [[]]
	with open(filename, 'r') as file:
		for line in file:
			if line.strip() == '':
				in_list.append([])
			else:
				in_list[-1].append(line.strip())
	return in_list

if __name__ == "__main__":
        main_2()
