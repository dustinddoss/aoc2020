import math
import collections
import time

class Rule():
	def __init__(self, rule_id, children_str):
		self.rule_id = rule_id
		if '"' in children_str:
			# children_str must be '"x"' for some char x
			self.terminal_char = children_str[1]
			self.child_rules = None
		else:
			self.terminal_char = None
			self.child_rules = [[int(c) for c in rule.strip().split(' ')] for rule in children_str.split('|')]

	def is_terminal(self):
		return (self.terminal_char is not None)


	def match(self, input_str, rules_map):
		'''
		 Recursively applies the grammar defined by this rule on the input string from left to right
		 Input:
		    input_str: a text string to be matched against
		    rules_map: a map {int -> Rule} of all of the potential rules in this grammar
		 Output:
		    all_matches: A set of ints corresponding to the lengths of the
		    			 left-justified substrings of input_str possibly generated
		    			 by this rule and its children.

			e.g. if this rule is 0: 5 | 6 
			with rules_map = {5: "a", 6: "b"}
			then this rule can generate "a" or "ab".

			Thus, 
			for input_str "abaaab", the output would be (1, 2), as both "a" and "ab" starts the input.
			for input_str "aabbbb", the output would be (1), as only "a" starts the input_str.
			for input_str "bbaaab", the output would be (), the empty set, as the input starts with neither "a" nor "ab".
		'''
		if self.is_terminal():
			if len(input_str) > 0 and input_str[0] == self.terminal_char:
				return set([1])
			else:
				return set()
		else:
			all_matches = set()
			for rule in self.child_rules: # child_rules is the list of |-separated rules; each rule consists of several rule_ids.
				match_set = set([0])
				is_match = True
				for rule_id in rule: # make sure we can match each rule in order
					new_matches_map = {}
					for input_index in match_set:
						# what are all the ways that the child rule in question can match the input?
						# We check starting at each index output by the previous rule_id.
						child_matches = rules_map[rule_id].match(input_str[input_index:], rules_map)
						if len(child_matches) > 0:
							new_matches_map[input_index] = child_matches

					# If we didn't find any way for this specific rule_id to match the input, the whole rule is a failure.
					if len(new_matches_map) == 0:
						is_match = False
						break
					else:
						# update the ongoing set of indicies to check against for the next rule_id
						new_match_set = set()
						for key, value in new_matches_map.items():
							for new_index in value:
								new_match_set.add(key + new_index)
						match_set = new_match_set

				# If all of the rule_ids for this child rule matched in sequence, add the potential lengths
				# to the set to be returned.
				if is_match:
					all_matches = all_matches.union(match_set)

			return all_matches

	def __repr__(self):
		s = 'Rule(' + str(self.rule_id) + ': '
		if self.is_terminal():
			s += '"' + self.terminal_char + '"'
		else:
			s += str(self.child_rules)
		s += ')'
		return s


def main_1():
	rule_str_list, message_list = read_file_groups('input19.txt')
	rules_map = {}
	for rule_str in rule_str_list:
		rule_id, children_str = rule_str.split(':')
		rule_id = int(rule_id)
		children_str = children_str.strip()
		rules_map[rule_id] = Rule(rule_id, children_str)

	valid_count = 0
	for message in message_list:
		# If we don't check that the length of the message was output,
		# we might get "successes" that are actually failures if only
		# part of the message was matched by the rule.
		if len(message) in rules_map[0].match(message, rules_map):
			print(message)
			valid_count += 1

	print(valid_count)

def main_2():
	rule_str_list, message_list = read_file_groups('input19.txt')
	rule_str_list.extend(["8: 42 | 42 8", "11: 42 31 | 42 11 31"])
	rules_map = {}
	for rule_str in rule_str_list:
		rule_id, children_str = rule_str.split(':')
		rule_id = int(rule_id)
		children_str = children_str.strip()
		rules_map[rule_id] = Rule(rule_id, children_str)

	valid_count = 0
	for message in message_list:
		if len(message) in rules_map[0].match(message, rules_map):
			valid_count += 1

	print(valid_count)

def parse_line(line):
	return line

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

def read_file_groups(filename):
	in_list = [[]]
	with open(filename, 'r') as file:
		for line in file:
			if line.strip() == '':
				in_list.append([])
			else:
				in_list[-1].append(line.strip())
	return in_list

if __name__ == "__main__":
        main_2()