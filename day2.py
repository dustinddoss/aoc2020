def main_1():
	in_list = read_file('./input2')
	valid_count = 0
	for line in in_list:
		entry = parse_input(line)
		char_count = entry[2].count(entry[1])
		if char_count >= entry[0][0] and char_count <= entry[0][1]:
			valid_count += 1
	print(valid_count)

def main_2():
	in_list = read_file('./input2')
	valid_count = 0
	for line in in_list:
		entry = parse_input(line)
		char_count = 0
		if entry[2][entry[0][0]-1] == entry[1]: char_count += 1
		if entry[2][entry[0][1]-1] == entry[1]: char_count += 1
		if char_count == 1: valid_count += 1
	print(valid_count)

def parse_input(line):
	entry = line.split(' ')  # ['min-max', 'char:', 'password']
	entry[0] = [int(x) for x in entry[0].split('-')]
	entry[1] = entry[1][0]
	return entry

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list


if __name__ == "__main__":
	main_2()