def main_1():
	in_list = read_file('./input3.txt')
	x = 0
	width = len(in_list[0])
	tree_count = 0
	for row in in_list:
		if row[x] == '#': tree_count += 1
		x = (x + 3) % width
	print(tree_count)

def main_2():
	in_list = read_file('./input3.txt')
	width = len(in_list[0])
	height = len(in_list)
	tree_count_product = 1
	slopes = [(1,1), (3,1), (5,1), (7,1), (1,2)]
	for slope in slopes:
		x = 0
		y = 0
		tree_count = 0
		while y < height:
			if in_list[y][x] == '#': tree_count += 1
			y += slope[1]
			x = (x + slope[0]) % width
		tree_count_product *= tree_count
	print(tree_count_product)

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list


if __name__ == "__main__":
	main_2()