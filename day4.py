def main():
	in_list = read_file('./input4.txt')
	parsed = parse_input(in_list)
	needed_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
	valid_count = 0

	for document in parsed:
		if validate_2(document): valid_count += 1

	print(valid_count)

def validate_1(document):
	needed_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
	for field in needed_fields:
		if field not in document.keys():
			return False
	return True

'''

    byr (Birth Year) - four digits; at least 1920 and at most 2002.
    iyr (Issue Year) - four digits; at least 2010 and at most 2020.
    eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
    hgt (Height) - a number followed by either cm or in:
        If cm, the number must be at least 150 and at most 193.
        If in, the number must be at least 59 and at most 76.
    hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
    ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
    pid (Passport ID) - a nine-digit number, including leading zeroes.
    cid (Country ID) - ignored, missing or not.
'''
def validate_2(document):
	if not (validate_1(document)): return False
	false_key = ''
	false_count = 0
	try:
		byr = int(document['byr'])
		if len(document['byr']) != 4:
			return False
		if byr < 1920 or byr > 2002:
			false_key = 'byr'
			false_count += 1

		iyr = int(document['iyr'])
		if len(document['iyr']) != 4:
			return False
		if iyr < 2010 or iyr > 2020:
			false_key = 'iyr'
			false_count += 1
	    
		if len(document['eyr']) != 4:
			return False
		eyr = int(document['eyr'])
		if eyr < 2020 or eyr > 2030:
			false_key = 'eyr'
			false_count += 1

		inches = (document['hgt'][-2:] == 'in')
		hgt = int(document['hgt'][:-2])
		if (inches and (hgt < 59 or hgt > 76)) or (not inches and (hgt < 150 or hgt > 193)):
			false_key = 'hgt'
			false_count += 1

		hcl = document['hcl']
		hex_set = [str(x) for x in range(0, 10)] + ['a', 'b', 'c', 'd', 'e', 'f']
		if len(hcl) != 7:
			false_key = 'hcl'
			false_count += 1
		else:
			if hcl[0] != '#':
				false_count += 1
			for i in range(1, 7):
				if hcl[i] not in hex_set:
					false_key = 'hcl'
					false_count += 1
					break

		valid_ecl = 'amb blu brn gry grn hzl oth'.split(' ')
		ecl = document['ecl']
		if ecl not in valid_ecl:
			false_key = 'ecl'
			false_count += 1

		pid = document['pid']
		dec_set = [str(x) for x in range(0, 10)]
		if len(pid) != 9:
			false_key = 'pid'
			false_count += 1
		for char in pid:
			if not char in dec_set:
				false_key = 'pid'
				false_count += 1
				break

		if false_count == 1:
			print(str(document) + ',' + false_key)
		if false_count > 0:
			return False
	except ValueError:
		return False
	return True


def parse_input(in_list):
	entries = [{}]
	for line in in_list:
		if line == '':
			entries.append({})
		else:
			for field in line.split(' '):
				key, val = field.split(':')
				entries[-1][key] = val
	return entries

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

if __name__ == "__main__":
	main()