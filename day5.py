def main_1():
	in_list = read_file('./input5.txt')
	max_id = -1
	for line in in_list:
		row = get_row(line[0:7])
		col = get_col(line[7:])
		seat_id = 8*row + col
		if seat_id > max_id:
			max_id = seat_id
	print(max_id)

def main_2():
	ids = [8*get_row(line[0:7]) + get_col(line[7:]) for line in read_file('./input5.txt')]
	ids.sort()
	missing = 1
	for i in range(1, len(ids)):
		if ids[i-1] == ids[i] - 2:
			print(ids[i] - 1)



def get_row(row_str):
	assert len(row_str) == 7
	start = 0
	end = 127
	for c in row_str[0:6]:
		if c == 'F':
			end -= (end-start+1)//2
		else:
			start += (end-start+1)//2
	assert abs(start-end) <= 1
	return (start if row_str[-1] == 'F' else end)

def get_col(col_str):
	assert len(col_str) == 3
	start = 0
	end = 7
	for c in col_str[0:2]:
		if c == 'L':
			end -= (end-start+1)//2
		else:
			start += (end-start+1)//2
	assert abs(start-end) <= 1
	return (start if col_str[-1] == 'L' else end)


def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

if __name__ == "__main__":
	main_2()