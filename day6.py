def main_1():
	groups = read_file_groups('./input6.txt')
	total = 0
	for group in groups:
		charset = set()
		for line in group:
			for char in line:
				charset.add(char)
		total += len(charset)
	print(total)

def main_2():
	groups = read_file_groups('./input6.txt')
	total = 0
	for group in groups:
		charset = set(group[0])
		for line in group[1:]:
			charset = charset.intersection(set(line))
		total += len(charset)
	print(total)

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

def read_file_groups(filename):
	in_list = [[]]
	with open(filename, 'r') as file:
		for line in file:
			if line.strip() == '':
				in_list.append([])
			else:
				in_list[-1].append(line.strip())
	return in_list

if __name__ == "__main__":
	main_2()