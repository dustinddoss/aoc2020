def main_1():
	in_list = read_file('./input7.txt')
	rules_map = {} # key can go in all of [val]
	for rule_str in in_list:
		key, rules = parse_rule(rule_str)
		for rule in rules:
			if rule[0] not in rules_map:
				rules_map[rule[0]] = []
			rules_map[rule[0]].append(key)
	print(len(container_set(rules_map, 'shiny gold')))

def container_set(rules_map, bag_str):
	if bag_str not in rules_map:
		return set()

	ret_set = set(rules_map[bag_str])
	for container_str in rules_map[bag_str]:
		ret_set = ret_set.union(container_set(rules_map, container_str))

	return ret_set

def main_2():
	in_list = read_file('./input7.txt')
	rules_map = {} # key contains [(val, count)...]
	for rule_str in in_list:
		key, rules = parse_rule(rule_str)
		rules_map[key] = rules

	print(count_insides(rules_map, 'shiny gold'))

def count_insides(rules_map, bag_str):
	if bag_str not in rules_map:
		return 0

	count = 0
	for rule in rules_map[bag_str]:
		count += rule[1] * (1 + count_insides(rules_map, rule[0]))

	return count

def parse_rule(rule_str):
	rule_str_list = rule_str.split(' ')
	key = ' '.join(rule_str_list[0:2])
	rules = []
	for i in [4*x for x in range(1, len(rule_str_list) // 4)]:
		count = int(rule_str_list[i])
		val = ' '.join(rule_str_list[i+1:i+3])
		rules.append((val, count))
	return (key, rules)


def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

def read_file_groups(filename):
	in_list = [[]]
	with open(filename, 'r') as file:
		for line in file:
			if line.strip() == '':
				in_list.append([])
			else:
				in_list[-1].append(line.strip())
	return in_list

if __name__ == "__main__":
	main_2()