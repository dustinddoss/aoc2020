def main_1():
	in_list = read_file('./input8.txt')
	line_num_set = set()
	pointer = 0
	acc = 0
	while pointer not in line_num_set:
		line_num_set.add(pointer)
		inst, val = parse_line(in_list[pointer])
		if inst == 'nop':
			pointer += 1
		elif inst == 'acc':
			acc += val
			pointer += 1
		elif inst == 'jmp':
			pointer += val
	print(acc)

def main_2():
	in_list = read_file('./input8.txt')
	line_num_set = set()
	pointer = 0
	acc = 0
	swap_ptr = -1
	swap_acc = -1
	while True:
		if pointer >= len(in_list):
			break
		line_num_set.add(pointer)
		# Swap an instruction
		inst, val = parse_line(in_list[pointer])
		if inst == 'acc':
			acc += val
			pointer += 1
			continue
		else:
			# save current pointer and acc; attempt to swap NOP and ACC
			swap_ptr = pointer
			swap_acc = acc
			if inst == 'nop':
				pointer += val
			else:
				pointer += 1


		# Look for repeat or end
		while True:
			if pointer >= len(in_list):
				break
			if pointer in line_num_set:
				# go back and undo attempted swap
				pointer = swap_ptr
				acc = swap_acc
				inst, val = parse_line(in_list[pointer])
				if inst == 'nop':
					pointer += 1
				elif inst == 'jmp':
					pointer += val
				break

			# Continue normally if no repeat or end was found
			line_num_set.add(pointer)
			inst, val = parse_line(in_list[pointer])
			if inst == 'nop':
				pointer += 1
			elif inst == 'acc':
				acc += val
				pointer += 1
			elif inst == 'jmp':
				pointer += val
	print(acc)

def parse_line(line):
	instruction, val_str = line.split(' ')
	val = int(val_str[1:])
	if val_str[0] == '-':
		val *= -1
	return (instruction, val)

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

def read_file_groups(filename):
	in_list = [[]]
	with open(filename, 'r') as file:
		for line in file:
			if line.strip() == '':
				in_list.append([])
			else:
				in_list[-1].append(line.strip())
	return in_list

if __name__ == "__main__":
	main_2()