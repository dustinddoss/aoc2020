from collections import deque

def main_1():
	in_list = read_file('./input9.txt')
	print(get_missing_sum(in_list))

def main_2():
	in_list = read_file('./input9.txt')
	missing = get_missing_sum(in_list)
	sum_queue = deque()
	for i in range(len(in_list)):
		new_val = int(in_list[i])
		for j in range(len(sum_queue)):
			sum_queue[j] += new_val
		sum_queue.append(new_val)

		while len(sum_queue) > 0 and sum_queue[0] > missing:
			sum_queue.popleft()

		if len(sum_queue) > 1 and sum_queue[0] == missing:
			min_list = [int(x) for x in in_list[i-len(sum_queue)+1:i+1]]
			assert sum(min_list) == missing
			print(min(min_list) + max(min_list))


def get_missing_sum(in_list):
	input_queue = deque()
	sum_sets_queue = deque()
	i = 0
	while i < 25:
		sum_set = set()
		val1 = int(in_list[i])
		for val2 in input_queue:
			sum_set.add(val1+val2)
		input_queue.append(val1)
		sum_sets_queue.append(sum_set)
		i += 1

	while i < len(in_list):
		val1 = int(in_list[i])
		found_sum = False
		for sum_set in sum_sets_queue:
			if val1 in sum_set:
				found_sum = True
				break
		if not found_sum:
			return val1

		sum_set = set()
		sum_sets_queue.popleft()
		input_queue.popleft()

		for val2 in input_queue:
			sum_set.add(val1+val2)
		input_queue.append(val1)
		sum_sets_queue.append(sum_set)
		i += 1


def parse_line(line):
	pass

def read_file(filename):
	in_list = []
	with open(filename, 'r') as file:
		for line in file:
			in_list.append(line.strip())
	return in_list

def read_file_groups(filename):
	in_list = [[]]
	with open(filename, 'r') as file:
		for line in file:
			if line.strip() == '':
				in_list.append([])
			else:
				in_list[-1].append(line.strip())
	return in_list

if __name__ == "__main__":
	main_2()